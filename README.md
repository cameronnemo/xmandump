# cnemo.xmandump

Download manpages in XBPS packages, for usage with
[man.cgi(8)](https://man.openbsd.org/man.cgi.8).

## Usage

Initialize `xbps` in the working directory, initialize `xlocate`, then start
downloading manpages into the working directory:

```
$ xbps-install -S -r . --repository='https://alpha.de.repo.voidlinux.org/current'
$ xlocate -S
$ python3 unpack_changed.py
```

Behavior is controlled through the use of four environment variables:

* `XMANDUMP_ROOTDIR`: root directory passed to XBPS (default: '.')
* `XMANDUMP_CACHE`: JSON cache of packages and checksums (default: './cache.json')
* `XMANDUMP_DESTDIR`: destination for the downloaded manpages (default: '.')
* `XMANDUMP_REPO`: repository from which to query and download packages
(default: 'https://alpha.de.repo.voidlinux.org/current')

## Limitations

Does not handle multilib/debug/nonfree packages.

Quite slow if the repository is being accessed over the network (no parallel
requests).

Like `xlocate`, only supports `x86_64`.
