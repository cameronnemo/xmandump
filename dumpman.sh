#!/bin/sh

# Fetch a package from $XMANDOC_REPO with xbps-uhelper,
# Extract the package into a temporty directory with tar, and
# Merge the manpages into $XMANDOC_DESTDIR with cp -rfT

set -eu

pkg="$1"

: ${XMANDOC_DESTDIR:='.'}
: ${XMANDOC_ROOTDIR:='.'}
: ${XMANDOC_REPO:="https://alpha.de.repo.voidlinux.org/current"}

pkg_prop() {
	pkg="$1"
	prop="$2"
	xbps-query -r "${XMANDOC_ROOTDIR}" --repository="${XMANDOC_REPO}" \
		"-Rp${prop}" "${pkg}"
}

binpkg="$(pkg_prop "${pkg}" pkgver).$(pkg_prop "${pkg}" architecture).xbps"

tempdir="$(mktemp -d '/tmp/cnemo.xmandump.XXXXXX')"
trap 'rm -rf -- "${tempdir}"' EXIT
trap 'rm -rf -- "${tempdir}"; exit 0' TERM INT HUP

dir="$(pwd)"
cd "${tempdir}"
xbps-uhelper fetch "${XMANDOC_REPO}/${binpkg}" >/dev/null 2>&1
tar xf "${binpkg}"
cd "${dir}"

mkdir -p "${XMANDOC_DESTDIR}/usr/share/man"
cp -rfT "${tempdir}/usr/share/man" "${XMANDOC_DESTDIR}/usr/share/man"
