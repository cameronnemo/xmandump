import subprocess
import json
import sys
import os
from functools import reduce

_XLOCATE = '''xlocate '^/usr/share/man' | awk '{ print $1 }' | rev | cut -d'-' -f2- | rev | sort -u'''

def _xlocate(xquery):
    xloc = subprocess.run(['sh', '-c', _XLOCATE], check=True,
                          encoding='ascii', stdout=subprocess.PIPE)

    lines = filter(
        # don't query debug/multilib packages
        lambda x: not x.endswith('-dbg') and not x.endswith('-32bit'),
        xloc.stdout.splitlines()
    )

    for line in lines:
        xquery.send(line)

def _xquery(cachefilter):
    try:
        root = os.environ['XMANDUMP_ROOTDIR']
    except KeyError:
        root = '.'
    try:
        repo = os.environ['XMANDUMP_REPO']
    except KeyError:
        repo = 'https://alpha.de.repo.voidlinux.org/current'
    while True:
        pkgname = yield
        xq = subprocess.run(['xbps-query', '--repository=' + repo, '-r', root,
                             '-Rpfilename-sha256', pkgname], encoding='ascii',
                            stdout=subprocess.PIPE)
        try:
            xq.check_returncode()
            cachefilter.send((pkgname, xq.stdout.strip()))
        except subprocess.CalledProcessError:
            # ignore packages that can not be queried (e.g. nonfree)
            pass

def get_cache():
    try:
        return os.environ['XMANDUMP_CACHE']
    except KeyError:    
        return 'cache.json'

def load_cache(cache):
    if os.path.exists(cache):
        return json.loads(open(cache).read())
    return {}

def _cachefilter(cache, dumper):
    while True:
        pkgsha = yield
        #pkgsha = pkgsha.items()
        if pkgsha[0] not in cache.keys() or pkgsha[1] != cache[pkgsha[0]]:
        #if pkg not in cache.keys() or sha != cache[pkg]:
            dumper.send(pkgsha)

def _dumper(cacher):
    while True:
        pkgsha = yield
        # TODO: rewrite this in Python or other
        subprocess.run(['sh', 'dumpman.sh', pkgsha[0]], check=True)
        cacher.send(pkgsha)

def _cacher(cpath, cache):
    while True:
        pkgsha = yield
        cache.update({pkgsha[0]: pkgsha[1]})
        open(cpath, 'w').write(json.dumps(cache))

def build_pipeline():
    cpath = get_cache()
    cache = load_cache(cpath)
    cacher = _cacher(cpath, cache)
    next(cacher)
    dumper = _dumper(cacher)
    next(dumper)
    cachefilter = _cachefilter(cache, dumper)
    next(cachefilter)
    xquery = _xquery(cachefilter)
    next(xquery)
    xlocate = _xlocate(xquery)

if __name__ == '__main__':
    build_pipeline()
